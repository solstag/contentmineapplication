{
  "id": [
    "25693791"
  ],
  "source": [
    "MED"
  ],
  "pmid": [
    "25693791"
  ],
  "pmcid": [
    "PMC4455224"
  ],
  "doi": [
    "10.1002/14651858.cd008152.pub4"
  ],
  "title": [
    "Primaquine or other 8-aminoquinoline for reducing Plasmodium falciparum transmission."
  ],
  "authorString": [
    "Graves PM, Gelband H, Garner P."
  ],
  "authorList": [
    {
      "author": [
        {
          "fullName": [
            "Graves PM"
          ],
          "firstName": [
            "Patricia M"
          ],
          "lastName": [
            "Graves"
          ],
          "initials": [
            "PM"
          ],
          "affiliation": [
            "College of Public Health, Medical and Veterinary Sciences, James Cook University, PO Box 6811, Cairns, Queensland, Australia, 4870. pgraves.work@gmail.com. patricia.graves@jcu.edu.au."
          ]
        },
        {
          "fullName": [
            "Gelband H"
          ],
          "firstName": [
            "Hellen"
          ],
          "lastName": [
            "Gelband"
          ],
          "initials": [
            "H"
          ]
        },
        {
          "fullName": [
            "Garner P"
          ],
          "firstName": [
            "Paul"
          ],
          "lastName": [
            "Garner"
          ],
          "initials": [
            "P"
          ]
        }
      ]
    }
  ],
  "journalInfo": [
    {
      "issue": [
        "2"
      ],
      "journalIssueId": [
        "2414072"
      ],
      "dateOfPublication": [
        "2015 "
      ],
      "monthOfPublication": [
        "0"
      ],
      "yearOfPublication": [
        "2015"
      ],
      "printPublicationDate": [
        "2015-01-01"
      ],
      "journal": [
        {
          "title": [
            "The Cochrane database of systematic reviews"
          ],
          "ISOAbbreviation": [
            "Cochrane Database Syst Rev"
          ],
          "medlineAbbreviation": [
            "Cochrane Database Syst Rev"
          ],
          "NLMid": [
            "100909747"
          ],
          "ESSN": [
            "1469-493X"
          ]
        }
      ]
    }
  ],
  "pageInfo": [
    "CD008152"
  ],
  "abstractText": [
    "Mosquitoes become infected with Plasmodium when they ingest gametocyte-stage parasites from an infected person's blood. Plasmodium falciparum gametocytes are sensitive to 8-aminoquinolines (8AQ), and consequently these drugs could prevent parasite transmission from infected people to mosquitoes and reduce the incidence of malaria. However, when used in this way, these drugs will not directly benefit the individual.In 2010, the World Health Organization (WHO) recommended a single dose of primaquine (PQ) at 0.75 mg/kg alongside treatment for P. falciparum malaria to reduce transmission in areas approaching malaria elimination. In 2013, the WHO revised this to 0.25 mg/kg to reduce risk of harms in people with G6PD deficiency.To assess the effects of PQ (or an alternative 8AQ) given alongside treatment for P. falciparum malaria on malaria transmission and on the occurrence of adverse events.We searched the following databases up to 5 January 2015: the Cochrane Infectious Diseases Group Specialized Register; the Cochrane Central Register of Controlled Trials (CENTRAL), published in The Cochrane Library (Issue 1, 2015); MEDLINE (1966 to 5 January 2015); EMBASE (1980 to 5 January 2015); LILACS (1982 to 5 January 2015); metaRegister of Controlled Trials (mRCT); and the WHO trials search portal using 'malaria*', 'falciparum', 'primaquine', 8-aminoquinoline and eight individual 8AQ drug names as search terms. In addition, we searched conference proceedings and reference lists of included studies, and contacted researchers and organizations.Randomized controlled trials (RCTs) or quasi-RCTs in children or adults, comparing PQ (or alternative 8AQ) as a single dose or short course alongside treatment for P. falciparum malaria, with the same malaria treatment given without PQ/8AQ.Two review authors independently screened all abstracts, applied inclusion criteria and extracted data. We sought evidence of an impact on transmission (community incidence), infectiousness (mosquitoes infected from humans) and potential infectiousness (gametocyte measures). We calculated the area under the curve (AUC) for gametocyte density over time for comparisons for which data were available. We sought data on haematological and other adverse effects, asexual parasite clearance time and recrudescence. We stratified the analysis by artemisinin and non-artemisinin treatments; and by PQ dose (low < 0.4 mg/kg; medium ≥ 0.4 to < 0.6 mg/kg; high ≥ 0.6 mg/kg). We used the GRADE approach to assess evidence quality.We included 17 RCTs and one quasi-RCT. Eight trials tested for G6PD status: six then excluded participants with G6PD deficiency, one included only those with G6PD deficiency, and one included all irrespective of status. The remaining 10 trials either did not report on whether they tested (eight trials), or reported that they did not test (two trials).Nine trials included study arms with artemisinin-based treatments and eleven included study arms with non-artemisinin-based treatments.Only one trial evaluated PQ given as a single dose of less than 0.4 mg/kg. PQ with artemisinin-based treatments: No trials evaluated effects on malaria transmission directly (incidence, prevalence or entomological inoculation rate) and none evaluated infectiousness to mosquitoes. For potential infectiousness, the proportion of people with detectable gametocytaemia on day eight was reduced by around two-thirds with the high dose PQ category (RR 0.29, 95% confidence interval (CI) 0.22 to 0.37; seven trials, 1380 participants, high quality evidence) and the medium dose PQ category (RR 0.30, 95% CI 0.16 to 0.56; one trial, 219 participants, moderate quality evidence). For the low dose category, the effect size was smaller and the 95% CIs include the possibility of no effect (dose: 0.1 mg/kg: RR 0.67, 95% CI 0.44 to 1.02; one trial, 223 participants, low quality evidence). Reductions in log(10)AUC estimates for gametocytaemia on days 1 to 43 with medium and high doses ranged from 24.3% to 87.5%. For haemolysis, one trial reported percent change in mean haemoglobin against baseline and did not detect a difference between the two arms (very low quality evidence). PQ with non-artemisinin treatments: No trials assessed effects on malaria transmission directly. Two small trials from the same laboratory in China evaluated infectiousness to mosquitoes, and reported that infectivity was eliminated on day 8 in 15/15 patients receiving high dose PQ compared to 1/15 in the control group (low quality evidence). For potential infectiousness, the proportion of people with detectable gametocytaemia on day 8 was reduced by three-fifths with high dose PQ category (RR 0.39, 95% CI 0.25 to 0.62; four trials, 186 participants, high quality evidence), and by around two-fifths with medium dose category (RR 0.60, 95% CI 0.49 to 0.75; one trial, 216 participants, high quality evidence), with no trial in the low dose PQ category reporting this outcome. Reduction in log(10)AUC for gametocytaemia days 1 to 43 were 24.3% and 27.1% for two arms in one trial giving medium dose PQ. No trials systematically sought evidence of haemolysis.Two trials evaluated the 8AQ bulaquine, and suggest the effects may be greater than PQ, but the small number of participants (N = 112) preclude a definite conclusion.In individual patients, PQ added to malaria treatments reduces gametocyte prevalence, but this is based on trials using doses of more than 0.4 mg/kg. Whether this translates into preventing people transmitting malaria to mosquitoes has rarely been tested in controlled trials, but there appeared to be a strong reduction in infectiousness in the two small studies that evaluated this. No included trials evaluated whether this policy has an impact on community malaria transmission.For the currently recommended low dose regimen, there is currently little direct evidence to be confident that the effect of reduction in gametocyte prevalence is preserved, or that it is safe in people with G6PD deficiency."
  ],
  "affiliation": [
    "College of Public Health, Medical and Veterinary Sciences, James Cook University, PO Box 6811, Cairns, Queensland, Australia, 4870. pgraves.work@gmail.com. patricia.graves@jcu.edu.au."
  ],
  "language": [
    "eng"
  ],
  "pubModel": [
    "Electronic"
  ],
  "pubTypeList": [
    {
      "pubType": [
        "Journal Article",
        "Meta-Analysis",
        "Review",
        "Research Support, Non-U.S. Gov't"
      ]
    }
  ],
  "meshHeadingList": [
    {
      "meshHeading": [
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Humans"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Plasmodium falciparum"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "DE"
                  ],
                  "qualifierName": [
                    "drug effects"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Malaria, Falciparum"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "PS"
                  ],
                  "qualifierName": [
                    "parasitology"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                },
                {
                  "abbreviation": [
                    "PC"
                  ],
                  "qualifierName": [
                    "prevention & control"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                },
                {
                  "abbreviation": [
                    "TM"
                  ],
                  "qualifierName": [
                    "transmission"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Glucosephosphate Dehydrogenase Deficiency"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "DI"
                  ],
                  "qualifierName": [
                    "diagnosis"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Sulfadoxine"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "AD"
                  ],
                  "qualifierName": [
                    "administration & dosage"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Artemisinins"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "AD"
                  ],
                  "qualifierName": [
                    "administration & dosage"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                },
                {
                  "abbreviation": [
                    "TU"
                  ],
                  "qualifierName": [
                    "therapeutic use"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Quinine"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "TU"
                  ],
                  "qualifierName": [
                    "therapeutic use"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Pyrimethamine"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "AD"
                  ],
                  "qualifierName": [
                    "administration & dosage"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Chloroquine"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "TU"
                  ],
                  "qualifierName": [
                    "therapeutic use"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Primaquine"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "AD"
                  ],
                  "qualifierName": [
                    "administration & dosage"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Mefloquine"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "TU"
                  ],
                  "qualifierName": [
                    "therapeutic use"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Drug Combinations"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Antimalarials"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "AD"
                  ],
                  "qualifierName": [
                    "administration & dosage"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Randomized Controlled Trials as Topic"
          ]
        }
      ]
    }
  ],
  "chemicalList": [
    {
      "chemical": [
        {
          "name": [
            "Drug Combinations"
          ],
          "registryNumber": [
            "0"
          ]
        },
        {
          "name": [
            "Antimalarials"
          ],
          "registryNumber": [
            "0"
          ]
        },
        {
          "name": [
            "Artemisinins"
          ],
          "registryNumber": [
            "0"
          ]
        },
        {
          "name": [
            "artemisinine"
          ],
          "registryNumber": [
            "9RMU91N5K2"
          ]
        },
        {
          "name": [
            "fanasil, pyrimethamine drug combination"
          ],
          "registryNumber": [
            "37338-39-9"
          ]
        },
        {
          "name": [
            "Chloroquine"
          ],
          "registryNumber": [
            "886U3H6UFF"
          ]
        },
        {
          "name": [
            "Sulfadoxine"
          ],
          "registryNumber": [
            "88463U4SM5"
          ]
        },
        {
          "name": [
            "Pyrimethamine"
          ],
          "registryNumber": [
            "Z3614QOX8W"
          ]
        },
        {
          "name": [
            "Quinine"
          ],
          "registryNumber": [
            "A7V27PHC7A"
          ]
        },
        {
          "name": [
            "Primaquine"
          ],
          "registryNumber": [
            "MVR3634GX1"
          ]
        },
        {
          "name": [
            "Mefloquine"
          ],
          "registryNumber": [
            "TML814419R"
          ]
        },
        {
          "name": [
            "artesunate"
          ],
          "registryNumber": [
            "60W3249T9M"
          ]
        }
      ]
    }
  ],
  "subsetList": [
    {
      "subset": [
        {
          "code": [
            "IM"
          ],
          "name": [
            "Index Medicus"
          ]
        }
      ]
    }
  ],
  "fullTextUrlList": [
    {
      "fullTextUrl": [
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "pdf"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC4455224?pdf=render"
          ]
        },
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "html"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC4455224"
          ]
        },
        {
          "availability": [
            "Subscription required"
          ],
          "availabilityCode": [
            "S"
          ],
          "documentStyle": [
            "doi"
          ],
          "site": [
            "DOI"
          ],
          "url": [
            "http://dx.doi.org/10.1002/14651858.CD008152.pub4"
          ]
        }
      ]
    }
  ],
  "commentCorrectionList": [
    {
      "commentCorrection": [
        {
          "id": [
            "24979199"
          ],
          "source": [
            "MED"
          ],
          "reference": [
            "Cochrane Database Syst Rev. 2014;(6):CD008152"
          ],
          "type": [
            "Update of"
          ],
          "orderIn": [
            "87"
          ]
        }
      ]
    }
  ],
  "isOpenAccess": [
    "Y"
  ],
  "inEPMC": [
    "Y"
  ],
  "inPMC": [
    "N"
  ],
  "hasPDF": [
    "Y"
  ],
  "hasBook": [
    "N"
  ],
  "hasSuppl": [
    "N"
  ],
  "citedByCount": [
    "4"
  ],
  "hasReferences": [
    "Y"
  ],
  "hasTextMinedTerms": [
    "Y"
  ],
  "hasDbCrossReferences": [
    "N"
  ],
  "hasLabsLinks": [
    "Y"
  ],
  "epmcAuthMan": [
    "N"
  ],
  "hasTMAccessionNumbers": [
    "Y"
  ],
  "tmAccessionTypeList": [
    {
      "accessionType": [
        "nct"
      ]
    }
  ],
  "dateOfCompletion": [
    "2015-06-30"
  ],
  "dateOfCreation": [
    "2015-03-02"
  ],
  "dateOfRevision": [
    "2016-06-02"
  ],
  "electronicPublicationDate": [
    "2015-02-19"
  ],
  "firstPublicationDate": [
    "2015-02-19"
  ],
  "luceneScore": [
    "NaN"
  ]
}