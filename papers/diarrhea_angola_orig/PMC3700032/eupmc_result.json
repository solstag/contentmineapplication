{
  "id": [
    "23826505"
  ],
  "source": [
    "MED"
  ],
  "pmid": [
    "23826505"
  ],
  "pmcid": [
    "PMC3700032"
  ],
  "doi": [
    "10.7189/jogh.03.010401"
  ],
  "title": [
    "Epidemiology and etiology of childhood pneumonia in 2010: estimates of incidence, severe morbidity, mortality, underlying risk factors and causative pathogens for 192 countries."
  ],
  "authorString": [
    "Rudan I, O'Brien KL, Nair H, Liu L, Theodoratou E, Qazi S, Lukšić I, Fischer Walker CL, Black RE, Campbell H, Child Health Epidemiology Reference Group (CHERG)."
  ],
  "authorList": [
    {
      "author": [
        {
          "fullName": [
            "Rudan I"
          ],
          "firstName": [
            "Igor"
          ],
          "lastName": [
            "Rudan"
          ],
          "initials": [
            "I"
          ],
          "affiliation": [
            "Centre for Population Health Sciences and Global Health Academy, University of Edinburgh Medical School, Edinburgh, Scotland, UK."
          ]
        },
        {
          "fullName": [
            "O'Brien KL"
          ],
          "firstName": [
            "Katherine L"
          ],
          "lastName": [
            "O'Brien"
          ],
          "initials": [
            "KL"
          ]
        },
        {
          "fullName": [
            "Nair H"
          ],
          "firstName": [
            "Harish"
          ],
          "lastName": [
            "Nair"
          ],
          "initials": [
            "H"
          ]
        },
        {
          "fullName": [
            "Liu L"
          ],
          "firstName": [
            "Li"
          ],
          "lastName": [
            "Liu"
          ],
          "initials": [
            "L"
          ]
        },
        {
          "fullName": [
            "Theodoratou E"
          ],
          "firstName": [
            "Evropi"
          ],
          "lastName": [
            "Theodoratou"
          ],
          "initials": [
            "E"
          ],
          "authorId": [
            {
              "_": "0000-0001-5887-9132",
              "$": {
                "type": "ORCID"
              }
            }
          ]
        },
        {
          "fullName": [
            "Qazi S"
          ],
          "firstName": [
            "Shamim"
          ],
          "lastName": [
            "Qazi"
          ],
          "initials": [
            "S"
          ]
        },
        {
          "fullName": [
            "Lukšić I"
          ],
          "firstName": [
            "Ivana"
          ],
          "lastName": [
            "Lukšić"
          ],
          "initials": [
            "I"
          ]
        },
        {
          "fullName": [
            "Fischer Walker CL"
          ],
          "firstName": [
            "Christa L"
          ],
          "lastName": [
            "Fischer Walker"
          ],
          "initials": [
            "CL"
          ]
        },
        {
          "fullName": [
            "Black RE"
          ],
          "firstName": [
            "Robert E"
          ],
          "lastName": [
            "Black"
          ],
          "initials": [
            "RE"
          ]
        },
        {
          "fullName": [
            "Campbell H"
          ],
          "firstName": [
            "Harry"
          ],
          "lastName": [
            "Campbell"
          ],
          "initials": [
            "H"
          ]
        },
        {
          "collectiveName": [
            "Child Health Epidemiology Reference Group (CHERG)"
          ]
        }
      ]
    }
  ],
  "authorIdList": [
    {
      "authorId": [
        {
          "_": "0000-0001-5887-9132",
          "$": {
            "type": "ORCID"
          }
        }
      ]
    }
  ],
  "journalInfo": [
    {
      "issue": [
        "1"
      ],
      "volume": [
        "3"
      ],
      "journalIssueId": [
        "2054124"
      ],
      "dateOfPublication": [
        "2013 Jun"
      ],
      "monthOfPublication": [
        "6"
      ],
      "yearOfPublication": [
        "2013"
      ],
      "printPublicationDate": [
        "2013-06-01"
      ],
      "journal": [
        {
          "title": [
            "Journal of global health"
          ],
          "ISOAbbreviation": [
            "J Glob Health"
          ],
          "medlineAbbreviation": [
            "J Glob Health"
          ],
          "NLMid": [
            "101578780"
          ],
          "ISSN": [
            "2047-2978"
          ],
          "ESSN": [
            "2047-2986"
          ]
        }
      ]
    }
  ],
  "pageInfo": [
    "010401"
  ],
  "abstractText": [
    "BACKGROUND: The recent series of reviews conducted within the Global Action Plan for Pneumonia and Diarrhoea (GAPPD) addressed epidemiology of the two deadly diseases at the global and regional level; it also estimated the effectiveness of interventions, barriers to achieving high coverage and the main implications for health policy. The aim of this paper is to provide the estimates of childhood pneumonia at the country level. This should allow national policy-makers and stakeholders to implement proposed policies in the World Health Organization (WHO) and UNICEF member countries. METHODS: WE CONDUCTED A SERIES OF SYSTEMATIC REVIEWS TO UPDATE PREVIOUS ESTIMATES OF THE GLOBAL, REGIONAL AND NATIONAL BURDEN OF CHILDHOOD PNEUMONIA INCIDENCE, SEVERE MORBIDITY, MORTALITY, RISK FACTORS AND SPECIFIC CONTRIBUTIONS OF THE MOST COMMON PATHOGENS: Streptococcus pneumoniae (SP), Haemophilus influenzae type B (Hib), respiratory syncytial virus (RSV) and influenza virus (flu). We distributed the global and regional-level estimates of the number of cases, severe cases and deaths from childhood pneumonia in 2010-2011 by specific countries using an epidemiological model. The model was based on the prevalence of the five main risk factors for childhood pneumonia within countries (malnutrition, low birth weight, non-exclusive breastfeeding in the first four months, solid fuel use and crowding) and risk effect sizes estimated using meta-analysis. FINDINGS: The incidence of community-acquired childhood pneumonia in low- and middle-income countries (LMIC) in the year 2010, using World Health Organization's definition, was about 0.22 (interquartile range (IQR) 0.11-0.51) episodes per child-year (e/cy), with 11.5% (IQR 8.0-33.0%) of cases progressing to severe episodes. This is a reduction of nearly 25% over the past decade, which is consistent with observed reductions in the prevalence of risk factors for pneumonia throughout LMIC. At the level of pneumonia incidence, RSV is the most common pathogen, present in about 29% of all episodes, followed by influenza (17%). The contribution of different pathogens varies by pneumonia severity strata, with viral etiologies becoming relatively less important and most deaths in 2010 caused by the main bacterial agents - SP (33%) and Hib (16%), accounting for vaccine use against these two pathogens. CONCLUSIONS: In comparison to 2000, the primary epidemiological evidence contributing to the models of childhood pneumonia burden has improved only slightly; all estimates have wide uncertainty bounds. Still, there is evidence of a decreasing trend for all measures of the burden over the period 2000-2010. The estimates of pneumonia incidence, severe morbidity, mortality and etiology, although each derived from different and independent data, are internally consistent - lending credibility to the new set of estimates. Pneumonia continues to be the leading cause of both morbidity and mortality for young children beyond the neonatal period and requires ongoing strategies and progress to reduce the burden further."
  ],
  "affiliation": [
    "Centre for Population Health Sciences and Global Health Academy, University of Edinburgh Medical School, Edinburgh, Scotland, UK."
  ],
  "language": [
    "eng"
  ],
  "pubModel": [
    "Print"
  ],
  "pubTypeList": [
    {
      "pubType": [
        "Journal Article"
      ]
    }
  ],
  "grantsList": [
    {
      "grant": [
        {
          "grantId": [
            "095831"
          ],
          "agency": [
            "Wellcome Trust"
          ],
          "orderIn": [
            "0"
          ]
        }
      ]
    }
  ],
  "fullTextUrlList": [
    {
      "fullTextUrl": [
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "pdf"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC3700032?pdf=render"
          ]
        },
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "html"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC3700032"
          ]
        },
        {
          "availability": [
            "Free"
          ],
          "availabilityCode": [
            "F"
          ],
          "documentStyle": [
            "pdf"
          ],
          "site": [
            "PubMedCentral"
          ],
          "url": [
            "http://www.pubmedcentral.nih.gov/picrender.fcgi?tool=EBI&pubmedid=23826505&action=stream&blobtype=pdf"
          ]
        },
        {
          "availability": [
            "Free"
          ],
          "availabilityCode": [
            "F"
          ],
          "documentStyle": [
            "html"
          ],
          "site": [
            "PubMedCentral"
          ],
          "url": [
            "http://www.pubmedcentral.nih.gov/articlerender.fcgi?tool=EBI&pubmedid=23826505"
          ]
        },
        {
          "availability": [
            "Subscription required"
          ],
          "availabilityCode": [
            "S"
          ],
          "documentStyle": [
            "doi"
          ],
          "site": [
            "DOI"
          ],
          "url": [
            "http://dx.doi.org/10.7189/jogh.03.010401"
          ]
        }
      ]
    }
  ],
  "isOpenAccess": [
    "Y"
  ],
  "inEPMC": [
    "Y"
  ],
  "inPMC": [
    "Y"
  ],
  "hasPDF": [
    "Y"
  ],
  "hasBook": [
    "N"
  ],
  "hasSuppl": [
    "Y"
  ],
  "citedByCount": [
    "36"
  ],
  "hasReferences": [
    "Y"
  ],
  "hasTextMinedTerms": [
    "Y"
  ],
  "hasDbCrossReferences": [
    "N"
  ],
  "hasLabsLinks": [
    "Y"
  ],
  "license": [
    "cc by"
  ],
  "epmcAuthMan": [
    "N"
  ],
  "hasTMAccessionNumbers": [
    "N"
  ],
  "dateOfCompletion": [
    "2013-07-05"
  ],
  "dateOfCreation": [
    "2013-07-04"
  ],
  "dateOfRevision": [
    "2014-02-24"
  ],
  "firstPublicationDate": [
    "2013-06-01"
  ],
  "luceneScore": [
    "NaN"
  ]
}