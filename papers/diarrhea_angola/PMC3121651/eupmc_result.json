{
  "id": [
    "21609473"
  ],
  "source": [
    "MED"
  ],
  "pmid": [
    "21609473"
  ],
  "pmcid": [
    "PMC3121651"
  ],
  "doi": [
    "10.1186/1475-2875-10-144"
  ],
  "title": [
    "Quinine, an old anti-malarial drug in a modern world: role in the treatment of malaria."
  ],
  "authorString": [
    "Achan J, Talisuna AO, Erhart A, Yeka A, Tibenderana JK, Baliraine FN, Rosenthal PJ, D'Alessandro U."
  ],
  "authorList": [
    {
      "author": [
        {
          "fullName": [
            "Achan J"
          ],
          "firstName": [
            "Jane"
          ],
          "lastName": [
            "Achan"
          ],
          "initials": [
            "J"
          ]
        },
        {
          "fullName": [
            "Talisuna AO"
          ],
          "firstName": [
            "Ambrose O"
          ],
          "lastName": [
            "Talisuna"
          ],
          "initials": [
            "AO"
          ]
        },
        {
          "fullName": [
            "Erhart A"
          ],
          "firstName": [
            "Annette"
          ],
          "lastName": [
            "Erhart"
          ],
          "initials": [
            "A"
          ]
        },
        {
          "fullName": [
            "Yeka A"
          ],
          "firstName": [
            "Adoke"
          ],
          "lastName": [
            "Yeka"
          ],
          "initials": [
            "A"
          ]
        },
        {
          "fullName": [
            "Tibenderana JK"
          ],
          "firstName": [
            "James K"
          ],
          "lastName": [
            "Tibenderana"
          ],
          "initials": [
            "JK"
          ]
        },
        {
          "fullName": [
            "Baliraine FN"
          ],
          "firstName": [
            "Frederick N"
          ],
          "lastName": [
            "Baliraine"
          ],
          "initials": [
            "FN"
          ]
        },
        {
          "fullName": [
            "Rosenthal PJ"
          ],
          "firstName": [
            "Philip J"
          ],
          "lastName": [
            "Rosenthal"
          ],
          "initials": [
            "PJ"
          ]
        },
        {
          "fullName": [
            "D'Alessandro U"
          ],
          "firstName": [
            "Umberto"
          ],
          "lastName": [
            "D'Alessandro"
          ],
          "initials": [
            "U"
          ]
        }
      ]
    }
  ],
  "journalInfo": [
    {
      "volume": [
        "10"
      ],
      "journalIssueId": [
        "1800997"
      ],
      "dateOfPublication": [
        "2011 "
      ],
      "monthOfPublication": [
        "0"
      ],
      "yearOfPublication": [
        "2011"
      ],
      "printPublicationDate": [
        "2011-01-01"
      ],
      "journal": [
        {
          "title": [
            "Malaria journal"
          ],
          "ISOAbbreviation": [
            "Malar. J."
          ],
          "medlineAbbreviation": [
            "Malar J"
          ],
          "NLMid": [
            "101139802"
          ],
          "ESSN": [
            "1475-2875"
          ]
        }
      ]
    }
  ],
  "pageInfo": [
    "144"
  ],
  "abstractText": [
    "Quinine remains an important anti-malarial drug almost 400 years after its effectiveness was first documented. However, its continued use is challenged by its poor tolerability, poor compliance with complex dosing regimens, and the availability of more efficacious anti-malarial drugs. This article reviews the historical role of quinine, considers its current usage and provides insight into its appropriate future use in the treatment of malaria. In light of recent research findings intravenous artesunate should be the first-line drug for severe malaria, with quinine as an alternative. The role of rectal quinine as pre-referral treatment for severe malaria has not been fully explored, but it remains a promising intervention. In pregnancy, quinine continues to play a critical role in the management of malaria, especially in the first trimester, and it will remain a mainstay of treatment until safer alternatives become available. For uncomplicated malaria, artemisinin-based combination therapy (ACT) offers a better option than quinine though the difficulty of maintaining a steady supply of ACT in resource-limited settings renders the rapid withdrawal of quinine for uncomplicated malaria cases risky. The best approach would be to identify solutions to ACT stock-outs, maintain quinine in case of ACT stock-outs, and evaluate strategies for improving quinine treatment outcomes by combining it with antibiotics. In HIV and TB infected populations, concerns about potential interactions between quinine and antiretroviral and anti-tuberculosis drugs exist, and these will need further research and pharmacovigilance."
  ],
  "affiliation": [
    "Department of Pediatrics and Child Health, Makerere University College of Health Sciences, P.O. Box 7475, Kampala, Uganda. achanj@yahoo.co.uk"
  ],
  "language": [
    "eng"
  ],
  "pubModel": [
    "Electronic"
  ],
  "pubTypeList": [
    {
      "pubType": [
        "Historical Article",
        "Journal Article",
        "Review"
      ]
    }
  ],
  "meshHeadingList": [
    {
      "meshHeading": [
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Malaria"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "DT"
                  ],
                  "qualifierName": [
                    "drug therapy"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Quinine"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "HI"
                  ],
                  "qualifierName": [
                    "history"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                },
                {
                  "abbreviation": [
                    "PD"
                  ],
                  "qualifierName": [
                    "pharmacology"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                },
                {
                  "abbreviation": [
                    "TU"
                  ],
                  "qualifierName": [
                    "therapeutic use"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Antimalarials"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "HI"
                  ],
                  "qualifierName": [
                    "history"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                },
                {
                  "abbreviation": [
                    "PD"
                  ],
                  "qualifierName": [
                    "pharmacology"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                },
                {
                  "abbreviation": [
                    "TU"
                  ],
                  "qualifierName": [
                    "therapeutic use"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "History, 17th Century"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "History, 18th Century"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "History, 19th Century"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "History, 20th Century"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "History, 21st Century"
          ]
        }
      ]
    }
  ],
  "chemicalList": [
    {
      "chemical": [
        {
          "name": [
            "Antimalarials"
          ],
          "registryNumber": [
            "0"
          ]
        },
        {
          "name": [
            "Quinine"
          ],
          "registryNumber": [
            "130-95-0"
          ]
        }
      ]
    }
  ],
  "subsetList": [
    {
      "subset": [
        {
          "code": [
            "IM"
          ],
          "name": [
            "Index Medicus"
          ]
        }
      ]
    }
  ],
  "fullTextUrlList": [
    {
      "fullTextUrl": [
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "pdf"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC3121651?pdf=render"
          ]
        },
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "html"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC3121651"
          ]
        },
        {
          "availability": [
            "Free"
          ],
          "availabilityCode": [
            "F"
          ],
          "documentStyle": [
            "html"
          ],
          "site": [
            "PubMedCentral"
          ],
          "url": [
            "http://www.pubmedcentral.nih.gov/articlerender.fcgi?tool=EBI&pubmedid=21609473"
          ]
        },
        {
          "availability": [
            "Free"
          ],
          "availabilityCode": [
            "F"
          ],
          "documentStyle": [
            "pdf"
          ],
          "site": [
            "PubMedCentral"
          ],
          "url": [
            "http://www.pubmedcentral.nih.gov/picrender.fcgi?tool=EBI&pubmedid=21609473&action=stream&blobtype=pdf"
          ]
        },
        {
          "availability": [
            "Subscription required"
          ],
          "availabilityCode": [
            "S"
          ],
          "documentStyle": [
            "doi"
          ],
          "site": [
            "DOI"
          ],
          "url": [
            "http://dx.doi.org/10.1186/1475-2875-10-144"
          ]
        }
      ]
    }
  ],
  "isOpenAccess": [
    "Y"
  ],
  "inEPMC": [
    "Y"
  ],
  "inPMC": [
    "Y"
  ],
  "hasPDF": [
    "Y"
  ],
  "hasBook": [
    "N"
  ],
  "hasSuppl": [
    "N"
  ],
  "citedByCount": [
    "46"
  ],
  "hasReferences": [
    "Y"
  ],
  "hasTextMinedTerms": [
    "Y"
  ],
  "hasDbCrossReferences": [
    "N"
  ],
  "hasLabsLinks": [
    "Y"
  ],
  "license": [
    "cc by"
  ],
  "epmcAuthMan": [
    "N"
  ],
  "hasTMAccessionNumbers": [
    "N"
  ],
  "dateOfCompletion": [
    "2011-09-07"
  ],
  "dateOfCreation": [
    "2011-06-24"
  ],
  "dateOfRevision": [
    "2013-06-28"
  ],
  "electronicPublicationDate": [
    "2011-05-24"
  ],
  "firstPublicationDate": [
    "2011-05-24"
  ],
  "luceneScore": [
    "NaN"
  ]
}