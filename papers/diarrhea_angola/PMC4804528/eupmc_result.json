{
  "id": [
    "27005280"
  ],
  "source": [
    "MED"
  ],
  "pmid": [
    "27005280"
  ],
  "pmcid": [
    "PMC4804528"
  ],
  "doi": [
    "10.1186/s13012-016-0392-8"
  ],
  "title": [
    "Toward the sustainability of health interventions implemented in sub-Saharan Africa: a systematic review and conceptual framework."
  ],
  "authorString": [
    "Iwelunmor J, Blackstone S, Veira D, Nwaozuru U, Airhihenbuwa C, Munodawafa D, Kalipeni E, Jutal A, Shelley D, Ogedegebe G."
  ],
  "authorList": [
    {
      "author": [
        {
          "fullName": [
            "Iwelunmor J"
          ],
          "firstName": [
            "Juliet"
          ],
          "lastName": [
            "Iwelunmor"
          ],
          "initials": [
            "J"
          ],
          "affiliation": [
            "Department of Kinesiology and Community Health, College of Applied Health Sciences, University of Illinois Urbana-Champaign, Champaign, IL, USA. jiwez@illinois.edu."
          ]
        },
        {
          "fullName": [
            "Blackstone S"
          ],
          "firstName": [
            "Sarah"
          ],
          "lastName": [
            "Blackstone"
          ],
          "initials": [
            "S"
          ],
          "affiliation": [
            "Department of Kinesiology and Community Health, College of Applied Health Sciences, University of Illinois Urbana-Champaign, Champaign, IL, USA."
          ]
        },
        {
          "fullName": [
            "Veira D"
          ],
          "firstName": [
            "Dorice"
          ],
          "lastName": [
            "Veira"
          ],
          "initials": [
            "D"
          ],
          "affiliation": [
            "School of Medicine, New York University, New York, NY, USA."
          ]
        },
        {
          "fullName": [
            "Nwaozuru U"
          ],
          "firstName": [
            "Ucheoma"
          ],
          "lastName": [
            "Nwaozuru"
          ],
          "initials": [
            "U"
          ],
          "affiliation": [
            "Department of Kinesiology and Community Health, College of Applied Health Sciences, University of Illinois Urbana-Champaign, Champaign, IL, USA."
          ]
        },
        {
          "fullName": [
            "Airhihenbuwa C"
          ],
          "firstName": [
            "Collins"
          ],
          "lastName": [
            "Airhihenbuwa"
          ],
          "initials": [
            "C"
          ],
          "affiliation": [
            "Saint Louis University, Saint Louis, MO, USA."
          ]
        },
        {
          "fullName": [
            "Munodawafa D"
          ],
          "firstName": [
            "Davison"
          ],
          "lastName": [
            "Munodawafa"
          ],
          "initials": [
            "D"
          ],
          "affiliation": [
            "World Health Organization Regional Office for Africa, Brazzaville, Congo."
          ]
        },
        {
          "fullName": [
            "Kalipeni E"
          ],
          "firstName": [
            "Ezekiel"
          ],
          "lastName": [
            "Kalipeni"
          ],
          "initials": [
            "E"
          ],
          "affiliation": [
            "Department of Geography, University of Illinois Urbana-Champaign, Champaign, Il, USA."
          ]
        },
        {
          "fullName": [
            "Jutal A"
          ],
          "firstName": [
            "Antar"
          ],
          "lastName": [
            "Jutal"
          ],
          "initials": [
            "A"
          ],
          "affiliation": [
            "West Virginia University, Morgantown, WV, USA."
          ]
        },
        {
          "fullName": [
            "Shelley D"
          ],
          "firstName": [
            "Donna"
          ],
          "lastName": [
            "Shelley"
          ],
          "initials": [
            "D"
          ],
          "affiliation": [
            "School of Medicine, New York University, New York, NY, USA."
          ]
        },
        {
          "fullName": [
            "Ogedegebe G"
          ],
          "firstName": [
            "Gbenga"
          ],
          "lastName": [
            "Ogedegebe"
          ],
          "initials": [
            "G"
          ],
          "affiliation": [
            "School of Medicine, New York University, New York, NY, USA."
          ]
        }
      ]
    }
  ],
  "journalInfo": [
    {
      "volume": [
        "11"
      ],
      "journalIssueId": [
        "2401386"
      ],
      "dateOfPublication": [
        "2016 "
      ],
      "monthOfPublication": [
        "0"
      ],
      "yearOfPublication": [
        "2016"
      ],
      "printPublicationDate": [
        "2016-01-01"
      ],
      "journal": [
        {
          "title": [
            "Implementation science : IS"
          ],
          "ISOAbbreviation": [
            "Implement Sci"
          ],
          "medlineAbbreviation": [
            "Implement Sci"
          ],
          "NLMid": [
            "101258411"
          ],
          "ESSN": [
            "1748-5908"
          ]
        }
      ]
    }
  ],
  "pageInfo": [
    "43"
  ],
  "abstractText": [
    "Sub-Saharan Africa (SSA) is facing a double burden of disease with a rising prevalence of non-communicable diseases (NCDs) while the burden of communicable diseases (CDs) remains high. Despite these challenges, there remains a significant need to understand how or under what conditions health interventions implemented in sub-Saharan Africa are sustained. The purpose of this study was to conduct a systematic review of empirical literature to explore how health interventions implemented in SSA are sustained.We searched MEDLINE, Biological Abstracts, CINAHL, Embase, PsycInfo, SCIELO, Web of Science, and Google Scholar for available research investigating the sustainability of health interventions implemented in sub-Saharan Africa. We also used narrative synthesis to examine factors whether positive or negative that may influence the sustainability of health interventions in the region.The search identified 1819 citations, and following removal of duplicates and our inclusion/exclusion criteria, only 41 papers were eligible for inclusion in the review. Twenty-six countries were represented in this review, with Kenya and Nigeria having the most representation of available studies examining sustainability. Study dates ranged from 1996 to 2015. Of note, majority of these studies (30 %) were published in 2014. The most common framework utilized was the sustainability framework, which was discussed in four of the studies. Nineteen out of 41 studies (46 %) reported sustainability outcomes focused on communicable diseases, with HIV and AIDS represented in majority of the studies, followed by malaria. Only 21 out of 41 studies had clear definitions of sustainability. Community ownership and mobilization were recognized by many of the reviewed studies as crucial facilitators for intervention sustainability, both early on and after intervention implementation, while social and ecological conditions as well as societal upheavals were barriers that influenced the sustainment of interventions in sub-Saharan Africa.The sustainability of health interventions implemented in sub-Saharan Africa is inevitable given the double burden of diseases, health care worker shortage, weak health systems, and limited resources. We propose a conceptual framework that draws attention to sustainability as a core component of the overall life cycle of interventions implemented in the region."
  ],
  "affiliation": [
    "Department of Kinesiology and Community Health, College of Applied Health Sciences, University of Illinois Urbana-Champaign, Champaign, IL, USA. jiwez@illinois.edu."
  ],
  "language": [
    "eng"
  ],
  "pubModel": [
    "Electronic"
  ],
  "pubTypeList": [
    {
      "pubType": [
        "Journal Article",
        "Review",
        "Research Support, N.I.H., Extramural"
      ]
    }
  ],
  "grantsList": [
    {
      "grant": [
        {
          "grantId": [
            "R03TW010081"
          ],
          "agency": [
            "FIC NIH HHS"
          ],
          "acronym": [
            "TW"
          ],
          "orderIn": [
            "0"
          ]
        }
      ]
    }
  ],
  "keywordList": [
    {
      "keyword": [
        "Sustainability",
        "Sub-Saharan Africa",
        "Health Interventions",
        "Implementations"
      ]
    }
  ],
  "subsetList": [
    {
      "subset": [
        {
          "code": [
            "IM"
          ],
          "name": [
            "Index Medicus"
          ]
        }
      ]
    }
  ],
  "fullTextUrlList": [
    {
      "fullTextUrl": [
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "pdf"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC4804528?pdf=render"
          ]
        },
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "html"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC4804528"
          ]
        },
        {
          "availability": [
            "Subscription required"
          ],
          "availabilityCode": [
            "S"
          ],
          "documentStyle": [
            "doi"
          ],
          "site": [
            "DOI"
          ],
          "url": [
            "http://dx.doi.org/10.1186/s13012-016-0392-8"
          ]
        }
      ]
    }
  ],
  "commentCorrectionList": [
    {
      "commentCorrection": [
        {
          "id": [
            "27089925"
          ],
          "source": [
            "MED"
          ],
          "reference": [
            "Implement Sci. 2016;11(1):53"
          ],
          "type": [
            "Erratum in"
          ],
          "orderIn": [
            "59"
          ]
        }
      ]
    }
  ],
  "isOpenAccess": [
    "Y"
  ],
  "inEPMC": [
    "Y"
  ],
  "inPMC": [
    "N"
  ],
  "hasPDF": [
    "Y"
  ],
  "hasBook": [
    "N"
  ],
  "hasSuppl": [
    "N"
  ],
  "citedByCount": [
    "1"
  ],
  "hasReferences": [
    "Y"
  ],
  "hasTextMinedTerms": [
    "Y"
  ],
  "hasDbCrossReferences": [
    "N"
  ],
  "hasLabsLinks": [
    "N"
  ],
  "license": [
    "cc by"
  ],
  "epmcAuthMan": [
    "N"
  ],
  "hasTMAccessionNumbers": [
    "N"
  ],
  "dateOfCreation": [
    "2016-03-23"
  ],
  "dateOfRevision": [
    "2016-03-25"
  ],
  "electronicPublicationDate": [
    "2016-03-23"
  ],
  "firstPublicationDate": [
    "2016-03-23"
  ],
  "luceneScore": [
    "NaN"
  ]
}