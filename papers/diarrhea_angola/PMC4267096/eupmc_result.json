{
  "id": [
    "25520791"
  ],
  "source": [
    "MED"
  ],
  "pmid": [
    "25520791"
  ],
  "pmcid": [
    "PMC4267096"
  ],
  "doi": [
    "10.7189/jogh.04.020401"
  ],
  "title": [
    "Community case management of childhood illness in sub-Saharan Africa - findings from a cross-sectional survey on policy and implementation."
  ],
  "authorString": [
    "Rasanathan K, Muñiz M, Bakshi S, Kumar M, Solano A, Kariuki W, George A, Sylla M, Nefdt R, Young M, Diaz T."
  ],
  "authorList": [
    {
      "author": [
        {
          "fullName": [
            "Rasanathan K"
          ],
          "firstName": [
            "Kumanan"
          ],
          "lastName": [
            "Rasanathan"
          ],
          "initials": [
            "K"
          ],
          "affiliation": [
            "UNICEF, New York, NY, USA."
          ]
        },
        {
          "fullName": [
            "Muñiz M"
          ],
          "firstName": [
            "Maria"
          ],
          "lastName": [
            "Muñiz"
          ],
          "initials": [
            "M"
          ],
          "affiliation": [
            "UNICEF, New York, NY, USA."
          ]
        },
        {
          "fullName": [
            "Bakshi S"
          ],
          "firstName": [
            "Salina"
          ],
          "lastName": [
            "Bakshi"
          ],
          "initials": [
            "S"
          ],
          "affiliation": [
            "UNICEF, New York, NY, USA."
          ]
        },
        {
          "fullName": [
            "Kumar M"
          ],
          "firstName": [
            "Meghan"
          ],
          "lastName": [
            "Kumar"
          ],
          "initials": [
            "M"
          ],
          "affiliation": [
            "UNICEF Eastern and Southern Africa Regional Office, Nairobi, Kenya."
          ]
        },
        {
          "fullName": [
            "Solano A"
          ],
          "firstName": [
            "Agnes"
          ],
          "lastName": [
            "Solano"
          ],
          "initials": [
            "A"
          ],
          "affiliation": [
            "UNICEF West and Central Africa Regional Office, Dakar, Senegal."
          ]
        },
        {
          "fullName": [
            "Kariuki W"
          ],
          "firstName": [
            "Wanjiku"
          ],
          "lastName": [
            "Kariuki"
          ],
          "initials": [
            "W"
          ],
          "affiliation": [
            "UNICEF, New York, NY, USA."
          ]
        },
        {
          "fullName": [
            "George A"
          ],
          "firstName": [
            "Asha"
          ],
          "lastName": [
            "George"
          ],
          "initials": [
            "A"
          ],
          "affiliation": [
            "Johns Hopkins Bloomberg School of Public Health, Baltimore, MD, USA."
          ]
        },
        {
          "fullName": [
            "Sylla M"
          ],
          "firstName": [
            "Mariame"
          ],
          "lastName": [
            "Sylla"
          ],
          "initials": [
            "M"
          ],
          "affiliation": [
            "UNICEF West and Central Africa Regional Office, Dakar, Senegal."
          ]
        },
        {
          "fullName": [
            "Nefdt R"
          ],
          "firstName": [
            "Rory"
          ],
          "lastName": [
            "Nefdt"
          ],
          "initials": [
            "R"
          ],
          "affiliation": [
            "UNICEF Eastern and Southern Africa Regional Office, Nairobi, Kenya."
          ]
        },
        {
          "fullName": [
            "Young M"
          ],
          "firstName": [
            "Mark"
          ],
          "lastName": [
            "Young"
          ],
          "initials": [
            "M"
          ],
          "affiliation": [
            "UNICEF, New York, NY, USA."
          ]
        },
        {
          "fullName": [
            "Diaz T"
          ],
          "firstName": [
            "Theresa"
          ],
          "lastName": [
            "Diaz"
          ],
          "initials": [
            "T"
          ],
          "affiliation": [
            "UNICEF, New York, NY, USA."
          ]
        }
      ]
    }
  ],
  "journalInfo": [
    {
      "issue": [
        "2"
      ],
      "volume": [
        "4"
      ],
      "journalIssueId": [
        "2232814"
      ],
      "dateOfPublication": [
        "2014 Dec"
      ],
      "monthOfPublication": [
        "12"
      ],
      "yearOfPublication": [
        "2014"
      ],
      "printPublicationDate": [
        "2014-12-01"
      ],
      "journal": [
        {
          "title": [
            "Journal of global health"
          ],
          "ISOAbbreviation": [
            "J Glob Health"
          ],
          "medlineAbbreviation": [
            "J Glob Health"
          ],
          "NLMid": [
            "101578780"
          ],
          "ISSN": [
            "2047-2978"
          ],
          "ESSN": [
            "2047-2986"
          ]
        }
      ]
    }
  ],
  "pageInfo": [
    "020401"
  ],
  "abstractText": [
    "BACKGROUND: Community case management (CCM) involves training, supporting, and supplying community health workers (CHWs) to assess, classify and manage sick children with limited access to care at health facilities, in their communities. This paper aims to provide an overview of the status in 2013 of CCM policy and implementation in sub-Saharan African countries. METHODS: We undertook a cross-sectional, descriptive, quantitative survey amongst technical officers in Ministries of Health and UNICEF offices in 2013. The survey aim was to describe CCM policy and implementation in 45 countries in sub-Saharan Africa, focusing on: CHW profile, CHW activities, and financing. RESULTS: 42 countries responded. 35 countries in sub-Saharan Africa reported implementing CCM for diarrhoea, 33 for malaria, 28 for pneumonia, 6 for neonatal sepsis, 31 for malnutrition and 28 for integrated CCM (treatment of 3 conditions: diarrhoea, malaria and pneumonia) - an increase since 2010. In 27 countries, volunteers were providing CCM, compared to 14 countries with paid CHWs. User fees persisted for CCM in 6 countries and mark-ups on commodities in 10 countries. Most countries had a national policy, memo or written guidelines for CCM implementation for diarrhoea, malaria and pneumonia, with 20 countries having this for neonatal sepsis. Most countries plan gradual expansion of CCM but many countries' plans were dependent on development partners. A large group of countries had no plans for CCM for neonatal sepsis. CONCLUSION: 28 countries in sub-Saharan Africa now report implementing CCM for pneumonia, diarrhoea and malaria, or \"iCCM\". Most countries have developed some sort of written basis for CCM activities, yet the scale of implementation varies widely, so a focus on implementation is now required, including monitoring and evaluation of performance, quality and impact. There is also scope for expansion for newborn care. Key issues include financing and sustainability (with development partners still providing most funding), gaps in data on CCM activities, and the persistence of user fees and mark-ups in several countries. National health management information systems should also incorporate CCM activities."
  ],
  "affiliation": [
    "UNICEF, New York, NY, USA."
  ],
  "language": [
    "eng"
  ],
  "pubModel": [
    "Print"
  ],
  "pubTypeList": [
    {
      "pubType": [
        "Journal Article"
      ]
    }
  ],
  "fullTextUrlList": [
    {
      "fullTextUrl": [
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "pdf"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC4267096?pdf=render"
          ]
        },
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "html"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC4267096"
          ]
        },
        {
          "availability": [
            "Subscription required"
          ],
          "availabilityCode": [
            "S"
          ],
          "documentStyle": [
            "doi"
          ],
          "site": [
            "DOI"
          ],
          "url": [
            "http://dx.doi.org/10.7189/jogh.04.020401"
          ]
        }
      ]
    }
  ],
  "isOpenAccess": [
    "Y"
  ],
  "inEPMC": [
    "Y"
  ],
  "inPMC": [
    "N"
  ],
  "hasPDF": [
    "Y"
  ],
  "hasBook": [
    "N"
  ],
  "hasSuppl": [
    "N"
  ],
  "citedByCount": [
    "6"
  ],
  "hasReferences": [
    "Y"
  ],
  "hasTextMinedTerms": [
    "Y"
  ],
  "hasDbCrossReferences": [
    "N"
  ],
  "hasLabsLinks": [
    "N"
  ],
  "license": [
    "cc by"
  ],
  "epmcAuthMan": [
    "N"
  ],
  "hasTMAccessionNumbers": [
    "N"
  ],
  "dateOfCompletion": [
    "2014-12-18"
  ],
  "dateOfCreation": [
    "2014-12-18"
  ],
  "dateOfRevision": [
    "2014-12-20"
  ],
  "firstPublicationDate": [
    "2014-12-01"
  ],
  "luceneScore": [
    "NaN"
  ]
}