{
  "id": [
    "24447996"
  ],
  "source": [
    "MED"
  ],
  "pmid": [
    "24447996"
  ],
  "pmcid": [
    "PMC3917280"
  ],
  "doi": [
    "10.3390/md12010394"
  ],
  "title": [
    "Dinophysis toxins: causative organisms, distribution and fate in shellfish."
  ],
  "authorString": [
    "Reguera B, Riobó P, Rodríguez F, Díaz PA, Pizarro G, Paz B, Franco JM, Blanco J."
  ],
  "authorList": [
    {
      "author": [
        {
          "fullName": [
            "Reguera B"
          ],
          "firstName": [
            "Beatriz"
          ],
          "lastName": [
            "Reguera"
          ],
          "initials": [
            "B"
          ],
          "affiliation": [
            "Spanish Institute of Oceanography (IEO), Oceanographic Centre of Vigo, Subida a Radio Faro 50, Vigo 36390, Spain. beatriz.reguera@vi.ieo.es."
          ]
        },
        {
          "fullName": [
            "Riobó P"
          ],
          "firstName": [
            "Pilar"
          ],
          "lastName": [
            "Riobó"
          ],
          "initials": [
            "P"
          ],
          "affiliation": [
            "Spanish Institute of Oceanography (IEO), Oceanographic Centre of Vigo, Subida a Radio Faro 50, Vigo 36390, Spain. pilar.riobo@vi.ieo.es."
          ]
        },
        {
          "fullName": [
            "Rodríguez F"
          ],
          "firstName": [
            "Francisco"
          ],
          "lastName": [
            "Rodríguez"
          ],
          "initials": [
            "F"
          ],
          "authorId": [
            {
              "_": "0000-0002-6918-4771",
              "$": {
                "type": "ORCID"
              }
            }
          ],
          "affiliation": [
            "Spanish Institute of Oceanography (IEO), Oceanographic Centre of Vigo, Subida a Radio Faro 50, Vigo 36390, Spain. francisco.rodriguez@vi.ieo.es."
          ]
        },
        {
          "fullName": [
            "Díaz PA"
          ],
          "firstName": [
            "Patricio A"
          ],
          "lastName": [
            "Díaz"
          ],
          "initials": [
            "PA"
          ],
          "affiliation": [
            "Spanish Institute of Oceanography (IEO), Oceanographic Centre of Vigo, Subida a Radio Faro 50, Vigo 36390, Spain. patricio.diaz@vi.ieo.es."
          ]
        },
        {
          "fullName": [
            "Pizarro G"
          ],
          "firstName": [
            "Gemita"
          ],
          "lastName": [
            "Pizarro"
          ],
          "initials": [
            "G"
          ],
          "affiliation": [
            "Spanish Institute of Oceanography (IEO), Oceanographic Centre of Vigo, Subida a Radio Faro 50, Vigo 36390, Spain. gemita.pizarro@ifop.cl."
          ]
        },
        {
          "fullName": [
            "Paz B"
          ],
          "firstName": [
            "Beatriz"
          ],
          "lastName": [
            "Paz"
          ],
          "initials": [
            "B"
          ],
          "affiliation": [
            "Spanish Institute of Oceanography (IEO), Oceanographic Centre of Vigo, Subida a Radio Faro 50, Vigo 36390, Spain. beapaz@uvigo.es."
          ]
        },
        {
          "fullName": [
            "Franco JM"
          ],
          "firstName": [
            "José M"
          ],
          "lastName": [
            "Franco"
          ],
          "initials": [
            "JM"
          ],
          "affiliation": [
            "Spanish Institute of Oceanography (IEO), Oceanographic Centre of Vigo, Subida a Radio Faro 50, Vigo 36390, Spain. jose.franco@vi.ieo.es."
          ]
        },
        {
          "fullName": [
            "Blanco J"
          ],
          "firstName": [
            "Juan"
          ],
          "lastName": [
            "Blanco"
          ],
          "initials": [
            "J"
          ],
          "affiliation": [
            "Spanish Institute of Oceanography (IEO), Oceanographic Centre of Vigo, Subida a Radio Faro 50, Vigo 36390, Spain. juan.blanco@cimacoron.org."
          ]
        }
      ]
    }
  ],
  "authorIdList": [
    {
      "authorId": [
        {
          "_": "0000-0002-6918-4771",
          "$": {
            "type": "ORCID"
          }
        }
      ]
    }
  ],
  "journalInfo": [
    {
      "issue": [
        "1"
      ],
      "volume": [
        "12"
      ],
      "journalIssueId": [
        "2125886"
      ],
      "dateOfPublication": [
        "2014 Jan"
      ],
      "monthOfPublication": [
        "1"
      ],
      "yearOfPublication": [
        "2014"
      ],
      "printPublicationDate": [
        "2014-01-01"
      ],
      "journal": [
        {
          "title": [
            "Marine drugs"
          ],
          "ISOAbbreviation": [
            "Mar Drugs"
          ],
          "medlineAbbreviation": [
            "Mar Drugs"
          ],
          "NLMid": [
            "101213729"
          ],
          "ESSN": [
            "1660-3397"
          ]
        }
      ]
    }
  ],
  "pageInfo": [
    "394-461"
  ],
  "abstractText": [
    "Several Dinophysis species produce diarrhoetic toxins (okadaic acid and dinophysistoxins) and pectenotoxins, and cause gastointestinal illness, Diarrhetic Shellfish Poisoning (DSP), even at low cell densities (<103 cells·L⁻¹). They are the main threat, in terms of days of harvesting bans, to aquaculture in Northern Japan, Chile, and Europe. Toxicity and toxin profiles are very variable, more between strains than species. The distribution of DSP events mirrors that of shellfish production areas that have implemented toxin regulations, otherwise misinterpreted as bacterial or viral contamination. Field observations and laboratory experiments have shown that most of the toxins produced by Dinophysis are released into the medium, raising questions about the ecological role of extracelular toxins and their potential uptake by shellfish. Shellfish contamination results from a complex balance between food selection, adsorption, species-specific enzymatic transformations, and allometric processes. Highest risk areas are those combining Dinophysis strains with high cell content of okadaates, aquaculture with predominance of mytilids (good accumulators of toxins), and consumers who frequently include mussels in their diet. Regions including pectenotoxins in their regulated phycotoxins will suffer from much longer harvesting bans and from disloyal competition with production areas where these toxins have been deregulated."
  ],
  "affiliation": [
    "Spanish Institute of Oceanography (IEO), Oceanographic Centre of Vigo, Subida a Radio Faro 50, Vigo 36390, Spain. beatriz.reguera@vi.ieo.es."
  ],
  "language": [
    "eng"
  ],
  "pubModel": [
    "Electronic"
  ],
  "pubTypeList": [
    {
      "pubType": [
        "Journal Article",
        "Review",
        "Research Support, Non-U.S. Gov't"
      ]
    }
  ],
  "meshHeadingList": [
    {
      "meshHeading": [
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Animals"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Humans"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Dinoflagellida"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "ME"
                  ],
                  "qualifierName": [
                    "metabolism"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                },
                {
                  "abbreviation": [
                    "CH"
                  ],
                  "qualifierName": [
                    "chemistry"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Marine Toxins"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "AN"
                  ],
                  "qualifierName": [
                    "analysis"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                },
                {
                  "abbreviation": [
                    "BI"
                  ],
                  "qualifierName": [
                    "biosynthesis"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                },
                {
                  "abbreviation": [
                    "ME"
                  ],
                  "qualifierName": [
                    "metabolism"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                },
                {
                  "abbreviation": [
                    "TO"
                  ],
                  "qualifierName": [
                    "toxicity"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Marine Biology"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Seawater"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "AN"
                  ],
                  "qualifierName": [
                    "analysis"
                  ],
                  "majorTopic_YN": [
                    "N"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Species Specificity"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Shellfish"
          ],
          "meshQualifierList": [
            {
              "meshQualifier": [
                {
                  "abbreviation": [
                    "AN"
                  ],
                  "qualifierName": [
                    "analysis"
                  ],
                  "majorTopic_YN": [
                    "Y"
                  ]
                }
              ]
            }
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Population"
          ]
        },
        {
          "majorTopic_YN": [
            "N"
          ],
          "descriptorName": [
            "Oceans and Seas"
          ]
        }
      ]
    }
  ],
  "chemicalList": [
    {
      "chemical": [
        {
          "name": [
            "Marine Toxins"
          ],
          "registryNumber": [
            "0"
          ]
        }
      ]
    }
  ],
  "subsetList": [
    {
      "subset": [
        {
          "code": [
            "IM"
          ],
          "name": [
            "Index Medicus"
          ]
        }
      ]
    }
  ],
  "fullTextUrlList": [
    {
      "fullTextUrl": [
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "pdf"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC3917280?pdf=render"
          ]
        },
        {
          "availability": [
            "Open access"
          ],
          "availabilityCode": [
            "OA"
          ],
          "documentStyle": [
            "html"
          ],
          "site": [
            "Europe_PMC"
          ],
          "url": [
            "http://europepmc.org/articles/PMC3917280"
          ]
        },
        {
          "availability": [
            "Free"
          ],
          "availabilityCode": [
            "F"
          ],
          "documentStyle": [
            "pdf"
          ],
          "site": [
            "PubMedCentral"
          ],
          "url": [
            "http://www.pubmedcentral.nih.gov/picrender.fcgi?tool=EBI&pubmedid=24447996&action=stream&blobtype=pdf"
          ]
        },
        {
          "availability": [
            "Free"
          ],
          "availabilityCode": [
            "F"
          ],
          "documentStyle": [
            "html"
          ],
          "site": [
            "PubMedCentral"
          ],
          "url": [
            "http://www.pubmedcentral.nih.gov/articlerender.fcgi?tool=EBI&pubmedid=24447996"
          ]
        },
        {
          "availability": [
            "Subscription required"
          ],
          "availabilityCode": [
            "S"
          ],
          "documentStyle": [
            "doi"
          ],
          "site": [
            "DOI"
          ],
          "url": [
            "http://dx.doi.org/10.3390/md12010394"
          ]
        }
      ]
    }
  ],
  "isOpenAccess": [
    "Y"
  ],
  "inEPMC": [
    "Y"
  ],
  "inPMC": [
    "Y"
  ],
  "hasPDF": [
    "Y"
  ],
  "hasBook": [
    "N"
  ],
  "hasSuppl": [
    "N"
  ],
  "citedByCount": [
    "16"
  ],
  "hasReferences": [
    "Y"
  ],
  "hasTextMinedTerms": [
    "Y"
  ],
  "hasDbCrossReferences": [
    "N"
  ],
  "hasLabsLinks": [
    "N"
  ],
  "license": [
    "cc by"
  ],
  "epmcAuthMan": [
    "N"
  ],
  "hasTMAccessionNumbers": [
    "N"
  ],
  "dateOfCompletion": [
    "2014-09-15"
  ],
  "dateOfCreation": [
    "2014-01-22"
  ],
  "electronicPublicationDate": [
    "2014-01-20"
  ],
  "firstPublicationDate": [
    "2014-01-20"
  ],
  "luceneScore": [
    "NaN"
  ]
}